#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  urlquery_driverScript.py
#  
#  Copyright 2017 Unknown <archie@acer>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os
import urllib
import json
try:
	import urllib.request as urllib2
except:
	import urllib2

running_version = 0

def wait_for_connection():
	while True:
		try:
			response = urllib2.urlopen('http://google.com').read()
			return
		except urllib2.URLError:
			pass

def check_version():
	url = "http://www.pssnitk.16mb.com/version.php"
	response = urllib2.urlopen(url).read()
	data = response.decode('UTF-8')
	current_version = json.loads(data)['version']
	return current_version


def main(args):
	wait_for_connection()
	current_version = int(check_version())	#check version
	if current_version > running_version:	#change gt to lt for stopping upgrade
		os.system('git reset --hard')
		os.system('git pull origin master')
		print('Version Updated')
	os.system('sudo python3 URLQuery.py')
	
	
if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
